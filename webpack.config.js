let path = require('path');
let ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
	entry: {
		'bundle.js': './assets/js/index.js',
		'bundle.css': './assets/css/index.js'
	},
	output: {
		filename: '[name]',
		path: path.resolve(__dirname, 'dist')
	},
	module: {
		rules: [
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: 'css-loader'
				})
			},
			{
				test: /\.vue$/,
				loader: 'vue-loader'
			}
		],
	},
	resolve: {
		alias: {
			'vue$': 'vue/dist/vue.esm.js'
		},
		extensions: ['*', '.js', '.vue', '.json']
	},
	plugins: [
		new ExtractTextPlugin("bundle.css"),
	]
};