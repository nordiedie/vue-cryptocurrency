import Vue from "vue";
import axios from "axios";

new Vue({
	el: '#app',
	data: {
		currencies: [],
		currentCurrency: {},
		showPreloader: true,
		currencyInfoPreloader: false,
		showCard: false,
		apiUrl: 'https://api.coinpaprika.com/v1/coins/'
	},
	mounted: function() {
		this.getCurrencies();
	},
	methods: {
		getCurrencies: function() {
			let self = this;
			axios.get(this.apiUrl)
				.then(function(currencies) {
					self.showPreloader = false;
					self.currencies = currencies.data;
				})
		},
		getCurrency: function(id) {
			let self = this;
			this.currencyInfoPreloader = true;
			this.showCard = false;
			axios.get(this.apiUrl + id)
				.then(function(currency) {
					self.showCard = true;
					self.currentCurrency = currency.data;
					self.currencyInfoPreloader = false;
				})
		}
	}
});